import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { OrdesService } from "../../../shared/services/ordes.service";

@Component({
	selector: 'app-orders-list',
	templateUrl: './orders-list.component.html',
	styleUrls: ['./orders-list.component.css']
})
export class OrdersListComponent implements OnInit {

	constructor( private orderSvc: OrdesService ) {  }

	all = [];
	dataSrc = new MatTableDataSource();

	@ViewChild( MatSort, { static: true }) sort: MatSort;

	ngOnInit() {
		this.getAllOrders();
	}

	getAllOrders(){
		this.orderSvc.getOrders().subscribe(resp => {
			this.all = this.dataSrc.data = resp;
			console.log('AllOrders=>', this.all)
		})
	}

	onChangeStatus(order: any){
		order.completed = true;
		this.orderSvc.updateOrder(order);
	}

	onDeleteOrder(id: string){
		this.orderSvc.deleteOrder(id);
	}
}