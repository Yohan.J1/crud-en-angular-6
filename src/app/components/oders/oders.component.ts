import { Component, OnInit } from '@angular/core';
import { OrdesService } from "../../shared/services/ordes.service";

@Component({
	selector: 'app-oders',
	templateUrl: './oders.component.html',
	styleUrls: ['./oders.component.css']
})
export class OdersComponent implements OnInit {

	constructor( private orderSvc: OrdesService ) { }

	products = [
		{
			name: "name 1",
			price: 1
		},
		{
			name: "name 2",
			price: 2
		},
		{
			name: "name 3",
			price: 3
		},
		{
			name: "name 4",
			price: 4
		},
		{
			name: "name 5",
			price: 5
		},
		{
			name: "name 6",
			price: 6
		}
	]

	appName: string = 'Food Good'
	totalOrders = 0;
	tempOrder = [];

	ngOnInit() {  }

	onAddProduct(product){
		this.totalOrders = (this.totalOrders + product.price);
		this.tempOrder.push(product.name)
	}

	deleteTempOrder = (order) => {
		let index = this.tempOrder.indexOf(order);
		if(index > -1) this.tempOrder.splice(index, 1)
	}

	onSubmit(){
		this.orderSvc.form.value.order = this.tempOrder;
		let data = this.orderSvc.form.value;
		data.totalOrders = this.tempOrder;

		this.orderSvc.createOrder(data);
		this.tempOrder = [];
		this.totalOrders = 0;
		this.orderSvc.form.reset();
	}
}
