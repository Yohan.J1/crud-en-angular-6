import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from "@angular/forms";
import { AngularFirestore, AngularFirestoreCollection } from "@angular/fire/firestore";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class OrdesService {
	
	private orderCollection: AngularFirestoreCollection<any>;
	
	orders: Observable<any[]>;

	constructor( private readonly afs: AngularFirestore) {
		this.orderCollection = afs.collection<any>('orders');
		this.orders = this.orderCollection.snapshotChanges()
			.pipe(map(actions => actions.map(a => {
				const data = a.payload.doc.data() as any;
				const id = a.payload.doc.id;
				return {id, ...data};
			})))
		}

	form = new FormGroup ({
		customerName: new FormControl(''),
		orderNumber: new FormControl(''),
		order: new FormControl(''),
		completed: new FormControl(false)
	});

	getOrders(){
		return this.orders;
	}

	updateOrder(order: any){
		return this.orderCollection.doc(order.id).update(order)
	}

	deleteOrder(id: string){
		return this.orderCollection.doc(id).delete();
	}

	createOrder(order: any){
		return this.orderCollection.add(order)
	}
}