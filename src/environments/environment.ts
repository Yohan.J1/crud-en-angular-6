// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
      apiKey: "AIzaSyCPjE17XbKwx5xZlsbtzcEHpC94AFPRgVs",
      authDomain: "barman-barismo.firebaseapp.com",
      databaseURL: "https://barman-barismo.firebaseio.com",
      projectId: "barman-barismo",
      storageBucket: "barman-barismo.appspot.com",
      messagingSenderId: "19078667871",
      appId: "1:19078667871:web:ff91048e608fca622045ea"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
